<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="{{ URL(config('landingpage.company.information.favicon')) }}" rel="shortcut icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">    
<meta name="keywords" content="">
<meta name="author" content="">
<link rel="stylesheet" href="{{ URL('assets/layouts/css/app.css') }}" />