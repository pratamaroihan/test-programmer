{{-- <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=["your-google-map-api"]&libraries=places"></script> --}}
<script src="{{ URL('assets/layouts/js/app.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $('#static-backdrop-modal-preview').show();
    $(window).on('load',function () {
        $('#static-backdrop-modal-preview').hide();
    });

    // jqa
    $(document).ready(function () {
        $('#static-backdrop-modal-preview').hide();
        $('form button[type=submit]').click(function (e) { 
            e.preventDefault();
            $(this).closest("form").submit();
            $('#static-backdrop-modal-preview').show();
        });
    });
</script>