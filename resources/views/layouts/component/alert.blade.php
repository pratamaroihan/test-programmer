@if (Session::has('status'))
<div class="grid grid-cols-12 gap-6">
    <div class="col-span-12 xl:col-span-12 xxl:col-span-12 z-10">
        <div class="mt-6 -mb-6 intro-y">
            <div class="alert alert-{{Session::get('status')}}-soft show flex items-center mb-2" role="alert"> 
                <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> {{Session::get('messages')}}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button> 
            </div>
        </div>
    </div>
</div>
@endif



