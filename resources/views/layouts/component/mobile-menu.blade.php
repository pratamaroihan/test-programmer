<div class="mobile-menu md:hidden">
    <div class="mobile-menu-bar">
        <a href="" class="flex mr-auto">
            <img alt="" class="w-6" src="{{ URL(config('landingpage.company.information.logo')) }}">
        </a>
        <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
    </div>
    <ul class="border-t border-theme-2 py-5 hidden">
        @can('be-beranda')
        <li>
            <a href="{{URL('/backend/beranda/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('beranda')}}">
                <div class="menu__icon"> <i data-feather="home"></i> </div>
                <div class="menu__title"> Beranda </div>
            </a>
        </li>
        <li class="menu__devider my-2"></li>
        @endcan

        @can('be-sm-forum') 
        <li>
            <a href="javascript:;" class="menu 
                menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('forum')}}
                menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('forum-list')}}
                    " >
                <div class="menu__icon"> <i data-feather="send"></i> </div>
                <div class="menu__title">
                    Forum
                    <div class="menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="
                    menu__{{ \App\Helpers\Services\MenuHelpers::activeMenu('forum') == 'active' ? 'sub-open' : ''}}
                    menu__{{ \App\Helpers\Services\MenuHelpers::activeMenu('forum-mylist') == 'active' ? 'sub-open' : ''}}
                    menu__{{ \App\Helpers\Services\MenuHelpers::activeMenu('forum-list') == 'active' ? 'sub-open' : ''}}
                    ">
                {{-- @can('be-sm-forum')
                    <li>
                        <a href="{{URL('/backend/forum')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('forum')}}">
                            <div class="menu__icon"> <i data-feather="send"></i> </div>
                            <div class="menu__title"> Forum </div>
                        </a>
                    </li>
                @endcan --}}
                @can('be-sm-forum-mylist')
                    <li>
                        <a href="{{URL('/backend/forum-mylist')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('forum-mylist')}}">
                            <div class="menu__icon"> <i data-feather="archive"></i> </div>
                            <div class="menu__title"> Forum My List </div>
                        </a>
                    </li>
                @endcan
                @can('be-sm-forum-list')
                    <li>
                        <a href="{{URL('/backend/forum-list')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('forum-list')}}">
                            <div class="menu__icon"> <i data-feather="archive"></i> </div>
                            <div class="menu__title"> Forum List </div>
                        </a>
                    </li>
                @endcan
            </ul>
        </li>
        <li class="nav__devider my-2"></li>
        @endcan
        @can('be-apps-generator')
        <li>
            <a href="javascript:;" class="menu 
            menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('file-manager')}} 
            menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('applications')}} 
            ">
                <div class="menu__icon"> <i data-feather="layers"></i> </div>
                <div class="menu__title"> Apps Generator <i data-feather="chevron-down" class="menu__sub-icon "></i> </div>
            </a>
            <ul class="
            menu__{{ \App\Helpers\Services\MenuHelpers::activeMenu('file-manager') == 'active' ? 'sub-open' : ''}} 
            menu__{{ \App\Helpers\Services\MenuHelpers::activeMenu('applications') == 'active' ? 'sub-open' : ''}} 
            ">
                @can('be-apps-generator-file-manager')
                <li>
                    <a href="{{URL('/backend/file-manager/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('file-manager')}}">
                        <div class="menu__icon"> <i data-feather="hard-drive"></i> </div>
                        <div class="menu__title"> File Manager </div>
                    </a>
                </li>
                @endcan
                @can('be-apps-generator-applications')
                <li>
                    <a href="{{URL('/backend/applications/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('applications')}}">
                        <div class="menu__icon"> <i data-feather="globe"></i> </div>
                        <div class="menu__title"> Applications </div>
                    </a>
                </li>
                @endcan
            </ul>
        </li>
        <li class="menu__devider my-2"></li>
        @endcan
        @can('be-master')
        <li>
            <a href="{{URL('/backend/master/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('master')}}">
                <div class="menu__icon"> <i data-feather="list"></i> </div>
                <div class="menu__title"> Data Master</div>
            </a>
        </li>
        <li class="menu__devider my-2"></li>
        @endcan
        @can('be-rbac')
        <li>
            <a href="{{URL('/backend/rbac/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('rbac')}}">
                <div class="menu__icon"> <i data-feather="users"></i> </div>
                <div class="menu__title"> Data RBAC</div>
            </a>
        </li>
        <li class="menu__devider my-2"></li>
        @endcan
        @can('be-landing-page')
        <li>
            <a href="javascript:;" class="menu 
            menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('landingpage')}} 
            ">
                <div class="menu__icon"> <i data-feather="layout"></i> </div>
                <div class="menu__title">
                    Landing Page 
                    <div class="menu__sub-icon "> 
                        <i data-feather="chevron-down"></i> 
                    </div>
                </div>
            </a>
            <ul class="
            menu__{{ \App\Helpers\Services\MenuHelpers::activeMenu('landingpage') == 'active' ? 'sub-open' : ''}} 
            ">
                <li>
                    <a href="{{URL('/backend/landingpage/basic-configuration/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('basic-configuration')}}">
                        <div class="menu__icon"> <i data-feather="circle"></i> </div>
                        <div class="menu__title"> Basic Config </div>
                    </a>
                </li>
                <li>
                    <a href="{{URL('/backend/landingpage/landing-page-section-config/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('landing-page-section-config')}}">
                        <div class="menu__icon"> <i data-feather="settings"></i> </div>
                        <div class="menu__title"> Section Config </div>
                    </a>
                </li>
                <li>
                    <a href="{{URL('/backend/landingpage/landing-page-section-data/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('landing-page-section-data')}}">
                        <div class="menu__icon"> <i data-feather="hard-drive"></i> </div>
                        <div class="menu__title"> Section Data </div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="menu__devider my-2"></li>
        @endcan
        <li>
            <a href="{{URL('/backend/manual-books/')}}" class="menu menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('manual-books')}}">
                <div class="menu__icon"> <i data-feather="book-open"></i> </div>
                <div class="menu__title"> Manual Book </div>
            </a>
        </li>

        <li class="menu__devider my-2"></li>
    </ul>
</div>