@extends('layouts.master')
@push('script-head')
@endpush
@push('title', 'Master Data Barang')
@push('name-content', 'Master Data Barang')
@section('content')
<div class="grid grid-cols-12 gap-6 mt-8">
    <div class="col-span-12 lg:col-span-12">
        <!-- BEGIN: Top Bar -->
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
            <form action="#" class="domain-transfer-form text-center">
                <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                    <div class="input-group"> 
                        <input type="text" class="form-control" placeholder="Search" aria-label="Price" name="search" aria-describedby="input-group-price" value="-">
                        <button type="submit" id="input-group-price" class="input-group-text btn btn-primary">
                            <i data-feather="search" class="w-5 h-5"></i>
                        </button>
                    </div>
                </div>
            </form>
            <div class="hidden md:block mx-auto text-gray-600">
                <div class="text-center">You have 12 entries</div>
                <div class="text-center">View all <a class="text-theme-10" href="#"><u>entries</u></a> </div>
            </div>
           
            <a href="#" class="btn btn-primary mr-2 mb-2 px-3 py-3 "> <i data-feather="plus" class="w-4 h-4 mr-2"></i> Tambah Data </a>

        </div>
        <!-- END: Top Bar -->

        <!-- BEGIN: Content -->
        {{-- Head --}}
        <div class="intro-y box px-4 py-4 mt-5">
            <div class="grid grid-cols-12 gap-6 font-medium">
                <div class="col-span-3 lg:col-span-3 xxl:col-span-3 contentHeader">
                    Nama Barang
                </div>
                <div class="col-span-3 lg:col-span-3 xxl:col-span-3 contentHeader">
                    Harga Satuan
                </div>
                <div class="col-span-3 lg:col-span-3 xxl:col-span-3 contentHeader">
                    Dibuat Pada
                </div>
                <div class="col-span-3 lg:col-span-3 xxl:col-span-3 contentHeaderAction">
                    Action
                </div>
            </div>
        </div>

        @foreach ($data_barang as $barang)
            
        <div class="intro-x box px-4 py-2 mt-2 zoom-in">
            <div class="grid grid-cols-12 gap-6">
                <div class="col-span-3 lg:col-span-3 xxl:col-span-3 contentData">
                    {{$barang->nama_barang}}
                </div>
                <div class="col-span-3 lg:col-span-3 xxl:col-span-3 contentData">
                    Rp. {{$barang->harga_satuan}}
                </div>
                <div class="col-span-3 lg:col-span-3 xxl:col-span-3 contentData">
                    {{date('d F Y', strtotime($barang->created_at))}}
                </div>
                <div class="col-span-3 lg:col-span-3 xxl:col-span-3 contentDataAction">
                    <a href="#" class="intro-y btn btn-secondary btn-sm mr-1"> 
                        <i data-feather="eye" class="w-5 h-5"></i> 
                    </a> 
                    <a href="#" class="intro-y btn btn-primary btn-sm mr-1"> 
                        <i data-feather="edit" class="w-5 h-5"></i> 
                    </a>
                    <a href="javascript:;" data-toggle="modal" data-target="#destroy{{$barang->id}}" class="intro-y btn btn-danger btn-sm mr-1"> 
                        <i data-feather="trash-2" class="w-5 h-5"></i> 
                    </a> 
                </div>
            </div>
        </div>
        <div id="destroy{{$barang->id}}" class="modal" data-backdrop="static" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body px-5 py-10">
                        <div class="text-center">
                            <form action="#" method="POST">
                                <i data-feather="trash-2" class="w-16 h-16 text-theme-24 mx-auto mt-3"></i> 
                                <div class="text-3xl mt-5">Are you sure?</div>
                                <div class="text-gray-600 mt-2">
                                    Do you really want to delete these records? 
                                    <br>
                                    This process cannot be undone.
                                </div>
                                <a href="{{URL('master/barang')}}" type="button" class="btn btn-secondary w-24 mr-2 mt-2">Cancel</a>
                                <button type="submit" data-dismiss="modal" class="btn btn-danger w-24 mr-2 mt-2">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        <div class="col-span-12 lg:col-span-12 xxl:col-span-12 mt-5">
            <div class="btn btn-light intro-x w-full block text-center rounded-md py-3"> {{$data_barang->links()}}</div>
        </div>
        <!-- End: Content -->
    </div>
</div>
@endsection
@push('script')
{{-- @include('backend::component.js.index') --}}
@endpush