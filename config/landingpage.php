<?php return array (
  'name' => 'LandingPage',
  'company' => 
  array (
    'information' => 
    array (
      'logo' => '/storage/COMPANY/ASSETS/LOGO/logo.png',
      'favicon' => '/storage/COMPANY/ASSETS/FAVICON/favicon.png',
      'short_name' => 'ASN Unggul',
      'long_name' => 'Pusat Teknologi Pengembangan Kompetensi LAN',
      'copyright_year' => '1 Jan, 2019',
      'email' => 'asn-unggul@lan.go.id',
      'phone' => '(021) 12345678',
      'fax' => '12345678',
      'address' => 'Jalan Veteran No.10, Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10110, Indonesia',
      'updated_by' => 1,
    ),
  ),
  'slider' => 
  array (
    'env' => 
    array (
      'interval' => '5',
      'opacity' => '1',
      'subject' => 
      array (
        'text_align' => 'center',
        'color' => '#fff',
        'font' => 'Courier New',
        'font_size' => 1.5,
        'size_type' => 'em',
        'weight' => 'normal',
        'style' => 'normal',
      ),
      'title' => 
      array (
        'text_align' => 'center',
        'color' => '#fff',
        'font' => 'Courier New',
        'font_size' => 1.5,
        'size_type' => 'em',
        'weight' => 'normal',
        'style' => 'normal',
      ),
      'tagline' => 
      array (
        'text_align' => 'center',
        'color' => '#fff',
        'font' => 'Courier New',
        'font_size' => 1.5,
        'size_type' => 'em',
        'weight' => 'normal',
        'style' => 'normal',
      ),
      'updated_by' => 1,
    ),
  ),
  'color' => 
  array (
    'env' => 
    array (
      'color_primary' => '#0f5c96',
      'color_secondary' => '#039BE5',
      'color_btn_primary' => '#2A4185',
      'color_btn_secondary' => '#039BE5',
      'color_text_btn_primary' => '#ffffff',
      'color_text_btn_secondary' => '#ffffff',
      'updated_by' => 1,
    ),
  ),
  'font' => 
  array (
    'env' => 
    array (
      'font_family' => 'Arial',
      'generic_family' => NULL,
      'updated_by' => 1,
    ),
  ),
  'navbar' => 
  array (
    'env' => 
    array (
      'navbar_display' => 'show',
      'navbar_background_color' => '#061950',
      'navbar_text_color' => '#ECF0F1',
      'updated_by' => 1,
    ),
  ),
  'section_search' => 
  array (
    'env' => 
    array (
      'search_title' => 'Ingin belajar apa?',
      'search_description' => 'Gunakan kata kunci untuk mencari materi pembelaajaran yang anda butuhkan',
      'search_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_moto' => 
  array (
    'env' => 
    array (
      'moto_title' => 'Section moto',
      'moto_description' => 'This is section moto',
      'moto_display' => 'hide',
      'updated_by' => 1,
    ),
  ),
  'section_profile' => 
  array (
    'env' => 
    array (
      'profile_title' => 'Section profile',
      'profile_description' => 'This is section profile',
      'profile_display' => 'hide',
      'updated_by' => 1,
    ),
  ),
  'section_service' => 
  array (
    'env' => 
    array (
      'service_title' => 'This is section service title',
      'service_description' => 'This is section service description',
      'service_display' => 'hide',
      'updated_by' => 1,
    ),
  ),
  'section_top_course' => 
  array (
    'env' => 
    array (
      'top_course_title' => 'Bahan Pembelajaran Terpopuler',
      'top_course_description' => 'Urutan bahan pembelajaran yang paling banyak digunakan',
      'top_course_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_new_course' => 
  array (
    'env' => 
    array (
      'new_course_title' => 'Materi Pembelajaran Terbaru',
      'new_course_description' => 'Materi pembelajaran terkini dari tenants ASN Unggul',
      'new_course_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_top_tenant' => 
  array (
    'env' => 
    array (
      'top_tenant_title' => 'Tenant Terpopuler',
      'top_tenant_description' => 'Tenant ASN Unggul yang memiliki peserta terdaftar paling banyak',
      'top_tenant_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_new_tenant' => 
  array (
    'env' => 
    array (
      'new_tenant_title' => 'Tenant Terbaru',
      'new_tenant_description' => 'Tenant yang baru saja bergabung ke ASN Unggul',
      'new_tenant_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_pertumbuhan_site' => 
  array (
    'env' => 
    array (
      'pertumbuhan_site_title' => 'Pertumbuhan Jaringan ASN Unggul',
      'pertumbuhan_site_description' => 'Mari bergotong-royong dalam gerakan mencerdaskan ASN Indonesia dengan menjadi bagian dari jaringan ASN Unggul',
      'pertumbuhan_site_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_faq' => 
  array (
    'env' => 
    array (
      'faq_title' => 'Pertanyaan Yang Sering Diajukan',
      'faq_description' => 'Sebelum menghubungi kami, periksa dulu jawaban pertanyaan atau solusi permasalahan anda di sini',
      'faq_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_pengumuman' => 
  array (
    'env' => 
    array (
      'pengumuman_title' => 'Informasi Penting',
      'pengumuman_description' => 'Jangan ketiinigalan informasi terkini, periksa secara reguler di sini',
      'pengumuman_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_kalender' => 
  array (
    'env' => 
    array (
      'kalender_title' => 'Agenda Penting',
      'kalender_description' => 'Jangan ketingggalan event penting, periksa secara reguler di sini',
      'kalender_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_testimonial' => 
  array (
    'env' => 
    array (
      'testimonial_title' => 'Berbagi Pengalaman',
      'testimonial_description' => 'Bagi kesan dan pesan anda dalam menggunakan ASN Unggul',
      'testimonial_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_maps' => 
  array (
    'env' => 
    array (
      'maps_title' => 'Alamat',
      'maps_description' => 'This is section maps description',
      'maps_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'section_footer' => 
  array (
    'env' => 
    array (
      'footer_name' => 'Administrasi Negara',
      'footer_copyright' => 'administrasi negara',
      'footer_display' => 'show',
      'updated_by' => 1,
    ),
  ),
  'card_top_course' => 
  array (
    'env' => 
    array (
      'type_card' => 'slider',
      'jumlah_kolom' => '4',
      'jumlah_baris' => '1',
      'updated_by' => 1,
    ),
  ),
  'card_new_course' => 
  array (
    'env' => 
    array (
      'type_card' => 'slider',
      'jumlah_kolom' => '4',
      'jumlah_baris' => '1',
      'updated_by' => 1,
    ),
  ),
  'card_top_tenant' => 
  array (
    'env' => 
    array (
      'type_card' => 'slider',
      'jumlah_kolom' => '4',
      'jumlah_baris' => '1',
      'updated_by' => 1,
    ),
  ),
  'card_new_tenant' => 
  array (
    'env' => 
    array (
      'type_card' => 'slider',
      'jumlah_kolom' => '4',
      'jumlah_baris' => '1',
      'updated_by' => 1,
    ),
  ),
  'card_pengumuman' => 
  array (
    'env' => 
    array (
      'type_card' => 'slider',
      'jumlah_kolom' => '4',
      'jumlah_baris' => '2',
      'updated_by' => 1,
    ),
  ),
  'card_testimonial' => 
  array (
    'env' => 
    array (
      'type_card' => 'no image',
      'updated_by' => 1,
    ),
  ),
  'card_maps' => 
  array (
    'env' => 
    array (
      'type_card' => 'small maps',
      'updated_by' => 1,
    ),
  ),
  'section_queque' => 
  array (
    'env' => 
    array (
      'search' => 
      array (
        'name' => 'search',
        'path' => 'landingpage::landingpage.content.search.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.search',
        'queque' => 3,
      ),
      'moto' => 
      array (
        'name' => 'moto',
        'path' => 'landingpage::landingpage.content.moto.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.moto',
        'queque' => 1,
      ),
      'company-description' => 
      array (
        'name' => 'company-description',
        'path' => 'landingpage::landingpage.content.company-description.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.company-description',
        'queque' => 2,
      ),
      'company-service' => 
      array (
        'name' => 'company-service',
        'path' => 'landingpage::landingpage.content.company-service.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.company-service',
        'queque' => 4,
      ),
      'popular-courses' => 
      array (
        'name' => 'popular-courses',
        'path' => 'landingpage::landingpage.content.courses.content.popular-courses.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.popular-courses',
        'queque' => 5,
      ),
      'new-courses' => 
      array (
        'name' => 'new-courses',
        'path' => 'landingpage::landingpage.content.courses.content.new-courses.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.new-courses',
        'queque' => 6,
      ),
      'popular-tenant' => 
      array (
        'name' => 'popular-tenant',
        'path' => 'landingpage::landingpage.content.tenant.content.popular-tenant.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.popular-tenant',
        'queque' => 7,
      ),
      'new-tenant' => 
      array (
        'name' => 'new-tenant',
        'path' => 'landingpage::landingpage.content.tenant.content.new-tenant.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.new-tenant',
        'queque' => 8,
      ),
      'chart' => 
      array (
        'name' => 'chart',
        'path' => 'landingpage::landingpage.content.chart.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.chart',
        'queque' => 9,
      ),
      'announcement' => 
      array (
        'name' => 'announcement',
        'path' => 'landingpage::landingpage.content.announcement.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.announcement',
        'queque' => 10,
      ),
      'faq' => 
      array (
        'name' => 'faq',
        'path' => 'landingpage::landingpage.content.faq.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.faq',
        'queque' => 12,
      ),
      'testimoni' => 
      array (
        'name' => 'testimoni',
        'path' => 'landingpage::landingpage.content.testimoni.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.testimoni',
        'queque' => 11,
      ),
      'calendar' => 
      array (
        'name' => 'calendar',
        'path' => 'landingpage::landingpage.content.calendar.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.calendar',
        'queque' => 13,
      ),
      'maps' => 
      array (
        'name' => 'maps',
        'path' => 'landingpage::landingpage.content.maps.content.index',
        'shortcut' => 'landingpage::landingpage.content.shortcut.component.maps',
        'queque' => 14,
      ),
    ),
  ),
);